package com.zhaoj.savemoney.ui;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.database.AccountDBHelper;
import com.zhaoj.savemoney.utils.ConstantsUtils;
import com.zhaoj.savemoney.widget.AccountItem;
import com.zhaoj.savemoney.widget.TopMenuNavbar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class AcountBookShelfActivity extends Activity {
	private static final String QUERY_ACCOUNT_SQL_STRING = "select * from account_tbl";
	public static long exitTime = 0;

	private GridView bookShelf;
	private TopMenuNavbar mTopMenuNavbar;
	private ShlefAdapter adapter;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_bookshelf);

		mTopMenuNavbar = (TopMenuNavbar) findViewById(R.id.book_top_menu_navbar);
		mTopMenuNavbar.tvTitle.setText(R.string.title_activity_book);
		mTopMenuNavbar.rightButton.setImageResource(R.drawable.icon_share);
		mTopMenuNavbar.leftButton.setVisibility(View.INVISIBLE);
		mTopMenuNavbar.rightButton
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						shareAccount();
						adapter.refresh();
					}
				});

		bookShelf = (GridView) findViewById(R.id.bookShelf);
		adapter = new ShlefAdapter();
		bookShelf.setAdapter(adapter);
		bookShelf.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ViewHolder holder = (ViewHolder) arg1.getTag();
				if (holder.accountID == ApplicationData.accountID) {
					adapter.refresh();
					return;
				}

				if (arg2 < adapter.accountItem.length) {
					if (holder.isShare) {
						holder.accountName
								.setBackgroundResource(R.drawable.book_share_cover_checked_txt);
					} else {
						holder.accountName
								.setBackgroundResource(R.drawable.book_cover_checked_txt);
					}
					openAcountDB(holder.accountPath, holder.accountName
							.getText().toString(), holder.accountID);
				} else {
					createAcountDB();
				}
				adapter.refresh();
			}

			private void openAcountDB(String dbPath, String dbNameString,
					int accountID) {
				// 打开
				ApplicationData.accountDBHelper.close();
				ApplicationData.accountDBHelper = new AccountDBHelper(
						AcountBookShelfActivity.this, dbPath, null,
						ApplicationData.VersionCode);
				// 记录
				SharedPreferences preferences = PreferenceManager
						.getDefaultSharedPreferences(AcountBookShelfActivity.this);
				SharedPreferences.Editor editor = preferences.edit();
				editor.putInt("account_id", accountID);
				editor.commit();
				ApplicationData.accountID = accountID;
				Toast.makeText(AcountBookShelfActivity.this,
						"打开了账本" + dbNameString, Toast.LENGTH_LONG).show();
			}

			private void createAcountDB() {
				final EditText dbNameEditText = new EditText(
						AcountBookShelfActivity.this);
				new AlertDialog.Builder(AcountBookShelfActivity.this)
						.setTitle("请输入账本名称")
						.setIcon(android.R.drawable.ic_dialog_info)
						.setView(dbNameEditText)
						.setPositiveButton("确定",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										String dbNameString = dbNameEditText
												.getText().toString();
										if (dbNameString.equals("")) {
											return;
										}
										ContentValues values = new ContentValues();
										values.clear();
										values.put("account_name", dbNameString);
										values.put(
												"account_path",
												ConstantsUtils
														.getDBFileName(dbNameString));
										values.put("account_isshare", 0);
										ApplicationData.accountManagerDBHelper
												.getWritableDatabase().insert(
														"account_tbl", null,
														values);
										Cursor cur = ApplicationData.accountManagerDBHelper
												.getWritableDatabase()
												.rawQuery(
														"select LAST_INSERT_ROWID() ",
														null);
										cur.moveToFirst();
										openAcountDB(ConstantsUtils
												.getDBFileName(dbNameString),
												dbNameString, cur.getInt(0));
										adapter.refresh();
									}
								}).setNegativeButton("取消", null).show();
			}
		});
	}

//	@Override
	protected void onResume() {
		super.onResume();
		adapter.refresh();
	}

	private void shareAccount() {
		AlertDialog.Builder builder = new Builder(AcountBookShelfActivity.this);
		builder.setMessage("将账本共享后，将可以多人共同记账，但将不能改为私有账本，是否确认修改？");
		builder.setTitle("确认");
		builder.setPositiveButton("确认", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				updateDBContent();
				dialog.dismiss();
			}
		});
		builder.setNegativeButton("取消", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.create().show();
	}

	private void updateDBContent() {
		ContentValues values = new ContentValues();
		values.put("account_isshare", 1);
		ApplicationData.accountManagerDBHelper.getWritableDatabase().update(
				"account_tbl", values, "account_id = ?",
				new String[] { String.valueOf(ApplicationData.accountID) });
	}

	class ShlefAdapter extends BaseAdapter {
		private AccountItem accountItem[];

		public ShlefAdapter() {
			initAccountItem();
		}

		public void refresh() {
			initAccountItem();
			notifyDataSetInvalidated();
			notifyDataSetChanged();
		}

		private void initAccountItem() {
			Cursor cursor;
			cursor = ApplicationData.accountManagerDBHelper
					.getReadableDatabase().rawQuery(QUERY_ACCOUNT_SQL_STRING,
							new String[] {});
			accountItem = new AccountItem[cursor.getCount()];
			int i = 0;
			while (cursor.moveToNext()) {
				accountItem[i++] = new AccountItem(
						cursor.getString(cursor.getColumnIndex("account_name")),
						cursor.getString(cursor.getColumnIndex("account_path")),
						cursor.getInt(cursor.getColumnIndex("account_isshare")),
						cursor.getInt(cursor.getColumnIndex("account_id")));
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return accountItem.length + 1;
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int position, View contentView, ViewGroup arg2) {
			ViewHolder holder;
			if (contentView == null) {
				holder = new ViewHolder();
				contentView = LayoutInflater.from(getApplicationContext())
						.inflate(R.layout.layout_bookitem, null);
				holder.accountName = (TextView) contentView
						.findViewById(R.id.imageView1);
				contentView.setTag(holder);
			} else {
				holder = (ViewHolder) contentView.getTag();
			}

			if (accountItem.length > position) {
				if (position < accountItem.length) {
					holder.accountName.setText(accountItem[position]
							.getmAccountName());
				}
				holder.isShare = accountItem[position].getmIsShare();
				holder.accountID = accountItem[position].getmAccountID();
				if (accountItem[position].getmIsShare()) {
					if (accountItem[position].getmAccountID() == ApplicationData.accountID) {
						holder.accountName
								.setBackgroundResource(R.drawable.book_share_cover_checked_txt);
					} else {
						holder.accountName
								.setBackgroundResource(R.drawable.book_share_cover_txt);
					}

				} else {
					if (accountItem[position].getmAccountID() == ApplicationData.accountID) {
						holder.accountName
								.setBackgroundResource(R.drawable.book_cover_checked_txt);
					} else {
						holder.accountName
								.setBackgroundResource(R.drawable.book_cover_txt);
					}

				}
				holder.accountPath = accountItem[position].getmAccountPath();
			} else {
				holder.accountName
						.setBackgroundResource(R.drawable.book_cover_add_txt);
				holder.accountName.setClickable(false);
				holder.accountName.setText("");
				// holder.accountName.setVisibility(View.INVISIBLE);
				holder.accountPath = "";
				holder.accountID = accountItem.length + 1;
			}
			return contentView;
		}
	}

	class ViewHolder {
		private int accountID;
		private boolean isShare;
		private String accountPath;
		private TextView accountName;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exit();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void exit() {
		if ((System.currentTimeMillis() - exitTime) > 20000) {
			Toast.makeText(
					ApplicationData.MainActivitycontext.getApplicationContext(),
					"再按一次退出程序", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			finish();
			System.exit(0);

		}
	}
}