package com.zhaoj.savemoney.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.zhaoj.savemoney.ApplicationData;
import com.zhaoj.savemoney.R;
import com.zhaoj.savemoney.service.UploadDataService;
import com.zhaoj.savemoney.utils.ConstantsUtils;
import com.zhaoj.savemoney.widget.DateMenuBar;
import com.zhaoj.savemoney.widget.DateMenuBar.OnDateStringChangedListener;
import com.zhaoj.savemoney.widget.DetailExListViewAdapter;
import com.zhaoj.savemoney.widget.TopMenuNavbar;

import android.annotation.SuppressLint;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class DetailActivity extends FragmentActivity implements
		OnClickListener, OnDateStringChangedListener {
	private static final int ADD_RECORD_REQUEST_CODE = 10000;
	private static final String SHORT_DATEFORMAT_STRING = "yyyy-MM-dd";
	public static long exitTime = 0;
	private TopMenuNavbar mTopMenuNavbar;
	private Intent mAddIntent;
	private DateMenuBar mDateMenuBar;
	private LocalActivityManager mlam;
	private TabHost mTabHost;
	private ExpandableListView mDetailListView;

	private DetailExListViewAdapter outAdapter;
	private DetailExListViewAdapter inAdapter;
	private DetailExListViewAdapter allAdapter;

	private boolean IsShowRemark = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		mlam = new LocalActivityManager(this, false);
		mlam.dispatchCreate(savedInstanceState);

		mTopMenuNavbar = (TopMenuNavbar) findViewById(R.id.top_menu_navbar);
		mTopMenuNavbar.rightButton.setOnClickListener(this);
		mTopMenuNavbar.leftButton.setOnClickListener(this);
		mAddIntent = new Intent(this, AddNewRecordActivity.class);

		mDateMenuBar = (DateMenuBar) findViewById(R.id.date_Menu_Bar);
		mDateMenuBar.leftButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDateMenuBar.curCalendar.add(Calendar.DAY_OF_MONTH, -1);
				mDateMenuBar.ReFreshDateFormat();
			}
		});

		mDateMenuBar.rightButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDateMenuBar.curCalendar.add(Calendar.DAY_OF_MONTH, 1);
				mDateMenuBar.ReFreshDateFormat();

			}
		});
		mDateMenuBar.setmOnDateChangedListener(this);

		InitAdapter();
		mTabHost = (TabHost) findViewById(R.id.tabhost);
		mTabHost.setup(mlam);

		LayoutInflater inflater_tab1 = getLayoutInflater();
		inflater_tab1.inflate(R.layout.layout_detaillist,
				mTabHost.getTabContentView());
		mTabHost.addTab(buildTabSpec("tab_all", R.string.detail_all, 0,
				R.id.detail_listview));
		mTabHost.addTab(buildTabSpec("tab_out", R.string.detail_out, 0,
				R.id.detail_listview));
		mTabHost.addTab(buildTabSpec("tab_in", R.string.detail_in, 0,
				R.id.detail_listview));
		updateTab(mTabHost);

		mDetailListView = (ExpandableListView) findViewById(R.id.detail_listview);

		mDetailListView.setAdapter(outAdapter);
		mTabHost.setCurrentTabByTag("tab_out");
		expandListViewGroup(outAdapter);
		mTabHost.setOnTabChangedListener(new OnTabChangeListener() {

			@Override
			public void onTabChanged(String tabId) {
				// TODO Auto-generated method stub
				if (tabId.equals("tab_all")) {
					mDetailListView.setAdapter(allAdapter);
					expandListViewGroup(allAdapter);
				} else if (tabId.equals("tab_out")) {
					mDetailListView.setAdapter(outAdapter);
					expandListViewGroup(outAdapter);
				} else if (tabId.equals("tab_in")) {
					mDetailListView.setAdapter(inAdapter);
					expandListViewGroup(inAdapter);
				}
			}
		});
	}

	private void expandListViewGroup(DetailExListViewAdapter adapter) {
		for (int i = 0; i < adapter.getGroupCount(); i++)
			mDetailListView.expandGroup(i);
	}

	@SuppressLint("SimpleDateFormat")
	private void InitAdapter() {
		String dateString;
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				SHORT_DATEFORMAT_STRING);
		dateString = dateFormat.format(mDateMenuBar.curCalendar.getTime());

		allAdapter = new DetailExListViewAdapter(this,
				ConstantsUtils.COST_KIND_ALL, IsShowRemark, dateString);
		outAdapter = new DetailExListViewAdapter(this,
				ConstantsUtils.COST_KIND_OUT, IsShowRemark, dateString);
		inAdapter = new DetailExListViewAdapter(this,
				ConstantsUtils.COST_KIND_IN, IsShowRemark, dateString);
	}

	private TabHost.TabSpec buildTabSpec(String tag, int resLabel, int resIcon,
			int content) {
		if (resIcon == 0) {
			return this.mTabHost.newTabSpec(tag)
					.setIndicator(getString(resLabel)).setContent(content);
		} else {
			return this.mTabHost
					.newTabSpec(tag)
					.setIndicator(getString(resLabel),
							getResources().getDrawable(resIcon))
					.setContent(content);
		}

	}

	private void updateTab(final TabHost tabHost) {
		for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
			TextView tv = (TextView) tabHost.getTabWidget().getChildAt(i)
					.findViewById(android.R.id.title);
			tv.setTextSize(16);
			tv.setTypeface(Typeface.SERIF, 2); // 设置字体和风格
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chart, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.left_button:
			IsShowRemark = !IsShowRemark;
			refreshAdapter();
			if (IsShowRemark) {
				mTopMenuNavbar.leftButton.setImageDrawable(getResources()
						.getDrawable(R.drawable.icon_day));
			} else {
				mTopMenuNavbar.leftButton.setImageDrawable(getResources()
						.getDrawable(R.drawable.icon_month));
			}
			break;
		case R.id.right_button: {
			startActivityForResult(mAddIntent, ADD_RECORD_REQUEST_CODE);
			break;
		}
		default:
			break;
		}
	}

	@SuppressLint("SimpleDateFormat")
	private void refreshAdapter() {
		String dateString;
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				SHORT_DATEFORMAT_STRING);
		dateString = dateFormat.format(mDateMenuBar.curCalendar.getTime());
		outAdapter.ReFresh(ConstantsUtils.COST_KIND_OUT, IsShowRemark,
				dateString);
		inAdapter.ReFresh(ConstantsUtils.COST_KIND_IN, IsShowRemark,
				dateString);
		allAdapter.ReFresh(ConstantsUtils.COST_KIND_ALL, IsShowRemark,
				dateString);
	}

	@Override
	protected void onResume() {
		refreshAdapter();
		super.onResume();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			exit();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void exit() {
		if ((System.currentTimeMillis() - exitTime) > 20000) {
			Toast.makeText(
					ApplicationData.MainActivitycontext.getApplicationContext(),
					"再按一次退出程序", Toast.LENGTH_SHORT).show();
			exitTime = System.currentTimeMillis();
		} else {
			finish();
			System.exit(0);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ADD_RECORD_REQUEST_CODE && resultCode == RESULT_OK) {
			refreshAdapter();
			if ((ApplicationData.mApi != null) && (ApplicationData.mApi.isAuthorized())) {
				Intent intent = new Intent(this, UploadDataService.class);
				startService(intent);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onDateStringChanged() {
		// TODO Auto-generated method stub
		refreshAdapter();
	}

}
