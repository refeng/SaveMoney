package com.zhaoj.savemoney.utils;

public class ConstantsUtils {
	public static final String DATABASE_PATH = android.os.Environment
			.getExternalStorageDirectory().getAbsolutePath() + "/SaveMoneyDB/";
	public static final String KUPAIN_PATH = "/SaveMoneyDB/";
	public final static String DEFAULT_DBNAME_STRING = "savemoney";
	public final static String ACCOUNT_MANAGER_DBNAME_STRING = "accountmanger";
	public final static String VR_DOWNLOAD_URL_STRING = "http://www.apk3.com/soft/download.asp?softid=690&downid=3&id=690";

	public static final int COST_KIND_OUT = 0;
	public static final int COST_KIND_IN = 1;
	public static final int COST_KIND_ALL = 2;

	final static public String ACCOUNT_PREFS_NAME = "prefs";
	final static public String ACCESS_KEY_NAME = "ACCESS_KEY";
	final static public String ACCESS_SECRET_NAME = "ACCESS_SECRET";
	final static public String ACCESS_AUTH_TYPE_NAME = "ACCESS_AUTH_TYPE_NAME";
	final static public String ACCESS_UID_NAME = "ACCESS_UID_NAME";

	public static String getDBFileName(String databaseFileName) {
		return ConstantsUtils.DATABASE_PATH + databaseFileName + ".db3";
	}
}
